The Jun's Quest
========================
#### Protagonista:
Chica adolescente de entre catorce y dieciseis años. Tez morena y ojos achinados.
	
#### CoProtagonistas:
+ Chicos y chicas de sus diversos círculos de amigos
  +   **Lisa**: Una de las mejores amigas de Jun
  +   **Fran**: La persona que le gustaría conocer a Jun
  +   **Carla**: Otra de las mejores amigas de Jun
+ **Hermano** (Ruby): Muy bueno con los ordenadores y la historia.
+ **Madre** (Linda): Programadora de ordenadores a tiempo parcial.
+ **Padre** (Christoper): Profesor de filosofía y otras asignaturas de letras como historia)
+ **Orthos**: suboficial del ejército de la sociedad secreta de la logia del club de caballeros: Los guardianes... de qué?... um... por determinar (reciben un cheque cada més para encargarse de sus cosas de guardianes y tal)
+ **Ocasius**: Capitán de primer rango del... este... ejército de la sociedad secrete de la logia del club de caballeros.. a, y el abuelo de Orthos. Últimamente le manda hacer casitodo a su subordinado (y nieto) porque vá el silla de ruedas (... más adelante se descubrirá que no la necesitaba, pero es que se está muy calentito con la manta y las alpargatas en una silla que se mueve con un mando)
+ **Gabriel Sterlin**: Famoso(?) arqueólogo(?) (¿Famoso y arqueólogo en la misma frase?)... ummm... arqueólogo de renombre reconocido más o menos porque 30 años antes fué el becario del REALMENTE famoso Doctor Degrass, descubridor de una misteriosa cultura muy antigua cerca de donde vive Jun (desgraciádamente su maestro, desapareció unos meses después del descubrimiento). Además, la poca fama que tiene actualmente en un canal de televisión se ve en peligro puesto que hay un grupo secreto que desea que averigüe donde se encuentra uno de los artefactos perdidos relacionados con la civilización que encontró su maestro. Es por eso que, durante buena parte de la historia, hará cosas de ética cuestionable para conseguir lo que Jun posee.
	
#### Lugar de los acontecimientos (localizaciones):
+ Un sitio parecido a Moaña

+ Un sitio parecido a Vigo (Centros comerciales con actividades que se cruzan con la historia)

+ Un sitio parecido a Pontevedra (Muséos y bibliotecas) [[<- o, mejor, un sitio parecido, para que no todo tenga que casar y, además, al no concretar los sitios se consigue que cada uno cree una imagen mental de la parte que no se explica.]]
	
#### Ideas para hacer el juego más completo:

+ Nombre del juego: **The Jun's Quest**

+ En primera instancia el nombre de la protagonista (Jun)  

+ Es ... algo así como subtítulo: ~~"The treasure quest"~~"The Jun's Quest"  (mejor que hacer referencia al tipo de tesoro que es... o que se imagina puede ser... me gusta más la idea de "disfrutar tanto del camino... de ahí lo que quest" que del propio hecho de encontrar el tesoro).  

+ Evidementemente habrá un momento (prácticamente al final del juego) en el que la protagonista y sus amigos, deberán decidir si lo que tienen en sus vidas y sus familias es más importante que lo que encuentren o hayan encontrado... y no hace falta que diga que, al igual que en los chicos de la baía de Goon... gana lo que se tiene por encima de lo que se desea, necesita, cree que es más importante. 

+ Se me ocurrió la idea de algo así como "un/a némesis" de la protagonista que es:  

  + Arqueólogo sumamente respetado (pero con una cara oculta conozida por muy poquitos). El malo, realmente, es un antihéroe que está siendo chantajeado por un extraño grupo de personas que buscan  que el "lago de la Serendipia" vuelva a funcionar (faltan pocas semanas para que se vuelvan a alinear los distintos elementos para que vuelva a ser útil) a fin de intervenir en eventos históricos recientes buscando únicamente su interés... Al final de la historia, el mismo «antagonista» se une a la causa de Jun y con «la sociedad secreta» (los graciosos) y sólo es, gracias al esfuerzo de todos que el lago de la Serencia no pueda ser utilizado para finel poco éticos)
			
+ Por otro lado, aun otra especie de Antagonista (aunque, es, realmente, para no tomárselo nada en serio y, en relativamente poco tiempo, dejará de ser antagonista para formar parte del grupo de amigos de Jun) que son "El Ejército de los Guardianes de los Caballeros de la Logia de la Serendipia" (EGCLS... para abreviar) compuesto por **Orthos** y **Ocasius**.
				
+ Especificar lo mínimo posible para que se genere un Lore basado en supuestos (que, bien unidos, tendrán más sentido)  

+ ES UNA AVENTURA GRÁFICA pero no estará demás añadir pequeños puzles en determinadas partes.  

+ Es más importante que la historia sea coherente y sea interesante Y DIVERTIDA que unos buenos gráficos.  

+ Una buena música ayuda mucho a dar un buen ambiente, habrá que empezar a pensar en volver a componer y preparar algún programa de composición (aunque sea MIDI).  

+ Los gráficos son bastante importante, pero no tengo prácticamente experiencia y, aunque la tuviera, mi creatividad puede ser limitante, intenta llegar con una idea bien clara de lo que quieres y tener avanzado el juego lo más posible (jugabilidad, historia y música) antes de buscar ayuda.    

+ Quizás, para hacerlo un poco más largo, se entera a la mitad, que sí objeto, no es el único que se perdió y que debe aunar fuerzas con el EGCLS para encontrar los otros trozos (son tres más y uno, lo tiene Gabriel)  

+ Otro de los objetos está en el mar/océano y otro relativamente cercano a una ciudad de un país dónde vive la familia de su madre (Desierto de Atacama?)  
  
 + El lago de la Serendipia lleva mucho tiempo secándose debido al mal uso de sus cualidades.    

 + Cada gema vista de noche en luna llena muestra diversos futuros divididos en bienes individuales, globales o, al contrario, futuros oscuros.
 
 + Ejemplos **históricos de buen uso**:
 
   + Fin de la paz entre dos ciudades estado de la antigua grecia porque las mujeres de los guerreros dejaron a un lado a sus maridos hasta que no se terminase la guerra (evidéntemente me refiero a lo acaecido en el relato griego "Lisístrata") 

 + Ejemplos **históricos de uso ambiguo**:

   + La llegada del hombre al nuevo continente (por determinar si fué bueno o malo)... de debió a una decisión crucial para poder llevar varias Carabelas en una ruta a las indias que podría costarles la vida a todos los marineros.  


 + Ejemplos **históricos de mal uso**:
 
   + La llegada al poder del nacionalsocialismo (se entiende que tendrá que ser relativamente difuminado porque la idea principal del juego es divertir no dar clases de historia con su, además, capacidad de ser leída de una u otra forma)  

   + La llegada al poder de determinados dictadores con el auspicio de países poderosos.  

   + La decisión de formar parte de *apoyos bélicos* para "liberar" países de los que, lo único que les importa es una determinada materia prima

+ *Mecánicas*:  

  + Para **guardar la partida**, Jun usará un diario que, prácticamente nunca sale de su cuarto, en algún momento el soldado de la EGCLS) lo sustrae y parte del juego de complicará porque Jun tiene que seguir una serie de pistas para volver a encontrarlo  
   
  + **Mapa circular**: Por definir por parte del GD  

  + **Accesos a atajos** mediante pequeños puzzles o acciones en principio irrelevantes del juego  
    +   ***Ejemplo del GD*** coches de policía bloqueando un atajo pero que, si se les da la noticia de que han de acudir a otra parte, se desbloqueará el atajo  
    + ***Idea del storyteller*** *Viaje submarino*: cuando estamos buscado otro de los objetos pedidos del lago de la Serendipia bajamos al fondo del mar y, en ese preciso momento las música será la de «viaje submarino»... Si se está el tiempo suficiente para escuchar la pieza entera, de desbloqueará un atajo
	
#### Historia general:

Jun (la protagonista) es una chica que hace poco se ha encontrado con un extraño objeto cuando caminaba por la playa con la perra de la familia (la perrita, de hecho... Y ESO ES MUY IMPORTANTE... le llevó el objeto colándose en una zona de la playa en la que había un cartel/advertencia de no pasar por pelígro químico/biológico/radioactivo o por prohibición expresa al estar excabando allí un arqueólogo muy famoso -que, más adelante, saldrá en la televisión hablando del sitio en el que Jun vive-).

  Lo guarda en casa y, durante un tiempo se olvida del objeto. El motivo principal porque Jun olvida el objeto es que, recibe petición de amistad de alguien que la agrada (y que tendrá cierta relevancia en la historia, pero no siempre como a Jun le hubiera gustado).
  
  Un día cercano al año nuevo, Jun, va con su familia a un Centro comercial en el que se encuentra con una performance en la que están representando parte de la batalla entre piratas y una armada de barcos del país en el que vive, lo curioso es que, en una de las imágenes que proyectan en una de las superficies, aparece un objeto muy similar al que ella tiene en casa. 

  En ese momento le pregunta a su hermano sobre qué sabe sobre la batalla y más concretamente sobre el objeto.
  
  Su hermano le cuenta lo poco que sabe lo mejor que puede (aunque, para darle  un poco más de duración a la historia, desgraciadamente la información o es muy poca o incorrecta).

  En algún momento después de la conversación con su hermano Jun y una amiga quedan a dormir en su casa (de Jun), ese día hay luna llena y el cielo está despejado, Lisa, la amiga de Jun, se despierta en la noche a por un vaso de agua pero tiene frío y empieza a escarbar en el armario de Jun para coger algo con lo que quitarse el frío, de repente, sin darse cuenta, se cae el extraño objeto que va dando vueltas sobre el suelo y acaba cerca de la ventana donde le da un pequeño rayo de luz de luna llena y, pasa algo extraordinario, el objeto empieza a emitir luz... Lisa avisa rápidamente a Jun y las dos contemplan absortas expectáculo, luces blancas y azules empiezan a bailar, pero, cuando bajan a enseñarles lo que ha pasado a su familia (los despiertan a todos super emocionados), una nube pasa por el cielo y debido a que ellas, en ese momento, no entienden porqué se empezó a iluminar, el objeto no hace nada más que estar ahí quieto, y, claro, su familia no ve con buenos ojos que les hayan despertado, el único que no ha ido es su padre (que es, sin lugar a dudas, por su conocimiento de historia, el que mejor podría haberlas ayudado).  
  
###### *(Idea sobre «el segundo punto de giro... O crisis»)* 
En un momento Jun se entera que a su hermano le pasa algo muy grave y que depende de que unos expertos decidan si ayudarle o no... Se da cuenta que, si consigue encontrar el objeto que les quitó «Gabriel» y encuentra ese sitio de leyenda llamado «el lago de la Serendia» podría ayudar a que los expertos lo elijan a él de entre los miles de interesados y luchará CASI hasta el final por conseguir del objeto perdido y la localización del lago